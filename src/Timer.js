import React, { Component } from "react";

class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            existMsg: "",
            displayErrBlock: false,
            localErr: false,
            localErrMsg: "",
            globalErr: false,
            globalErrMsg: ""
        };
        this.canvasRef = React.createRef();
    }

    componentDidMount() {
        this.setState(
            state => {
                let local = this.checkLocalTimer(),
                    global = this.checkGlobalTimer();
                const {localErr = false} = local;
                const {globalErr = false} = global;

                if (localErr || globalErr) {
                    return {
                        ...state,
                        ...global,
                        ...local,
                        ...{timerType: "exist", existMsg: "Error", displayErrBlock: true}
                    };
                } else {
                    return {...state, ...global, ...local};
                }
            },
            () => {
                console.log(this.state.timerType);
                this.canvasRender();
                if (this.state.timerType !== "exist") {
                    this.timerId = setInterval(this.timerFunction, 1000);
                }
            }
        );
    }

    componentDidUpdate() {
        this.canvasRender();
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    /**
     * рендерим таймер
     */
    canvasRender = () => {
        const {displayValue, timerType} = this.state;
        const {
            width = 400,
            height = 400,
            radius = 200,
            font = "18px sans-serif",
            textAlign = "center",
            textBaseline = "alphabetic",
            direction = "inherit",
            backColor,
            localColor,
            globalColor
        } = this.props.canvasOption;
        let hours = Math.floor(displayValue / 3600);
        let minutes = Math.floor((displayValue % 3600) / 60);
        let seconds = Math.floor((displayValue % 60) % 60);

        let canvas = this.canvasRef.current;
        let color = this.state.timerType === "global" ? globalColor : localColor;
        if (canvas !== null) {
            let ctx = canvas.getContext("2d");
            const {duration, leftValue: left} = this.state[ `${timerType}Timer` ];
            let rec = (left * 360) / duration;
            let begin = (rec * Math.PI) / 180 - 0.5 * Math.PI;
            ctx.clearRect(0, 0, width, height);
            //круг подложка
            ctx.beginPath();
            ctx.arc(
                width / 2,
                height / 2,
                radius - 10,
                -1.5 * Math.PI,
                1.5 * Math.PI
            );
            ctx.strokeStyle = backColor;
            ctx.lineWidth = "10";
            ctx.stroke();
            // основной круг
            ctx.beginPath();
            ctx.arc(width / 2, height / 2, radius - 10, begin, 1.5 * Math.PI);
            ctx.strokeStyle = color;
            ctx.fillText(`${hours}h  ${minutes}m ${seconds}s`, width / 2, height / 2);
            ctx.textAlign = textAlign;
            ctx.font = font;
            ctx.textBaseline = textBaseline;
            ctx.direction = direction;
            ctx.lineWidth = "12";
            ctx.stroke();
        }
    };

    /**
     * таймер
     */
    timerFunction = () => {
        const {timerType} = this.state;
        let {duration, leftValue} = this.state[ `${timerType}Timer` ];
        if (duration !== leftValue) {
            this.setState(state => ({
                [ `${timerType}Timer` ]: {
                    ...state[ `${timerType}Timer` ],
                    ...{leftValue: state[ `${timerType}Timer` ].leftValue + 1}
                },
                displayValue:
                    state[ `${timerType}Timer` ].duration -
                    (state[ `${timerType}Timer` ].leftValue + 1)
            }));
        } else {
            this.setTimerType(timerType);
        }
    };

    /**
     * устанавливаем тип таймера, после окончания предыдущего
     */
    setTimerType = timerType => {
        if (timerType === "local") {
            const {globalTimer} = this.state;
            let displayValue = globalTimer.duration - globalTimer.leftValue;
            this.setState({
                timerType: "global",
                displayValue
            });
        } else if (timerType === "global") {
            this.setState({
                timerType: "exist",
                existMsg: "Time is over!"
            });
            clearInterval(this.timerId);
        }
    };

    /**
     * проверяем данные в таймере
     */
    checkLocalTimer = () => {
        const {localTimer} = this.props;
        const {duration, leftValue} = localTimer;
        let localDuration = Math.abs(duration),
            localLeft = Math.abs(leftValue);
        if (localLeft < localDuration) {
            let displayValue = localDuration - localLeft;
            return {
                timerType: "local",
                localTimer: {
                    duration: localDuration,
                    leftValue: localLeft,
                },
                displayValue
            };
        } else if (localLeft === localDuration) {
            return {
                timerType: "global"
            };
        } else if (localLeft > localDuration) {
            return {
                localErr: "true",
                localErrMsg: "Check your data in local timer!"
            };
        }
    };

    /**
     * проверяем данные в глобальном таймере
     */
    checkGlobalTimer = () => {
        const {globalTimer} = this.props;
        const {duration, leftValue} = globalTimer;
        let globalDuration = Math.abs(duration),
            globalLeft = Math.abs(leftValue);

        if (globalLeft < globalDuration) {
            let displayValue = globalDuration - globalLeft;
            return {
                timerType: "global",
                globalTimer: {
                    duration: globalDuration,
                    leftValue: globalLeft
                },
                displayValue
            };
        } else if (leftValue === duration) {
            return {
                timerType: "exist",
                existMsg: "Time is over"
            };
        } else if (leftValue > duration) {
            return {
                globalErr: true,
                globalErrMsg: "Check your data in global timer!"
            };
        }
    };

    render() {
        const {
            id = "canvas",
            width = 400,
            height = 400
        } = this.props.canvasOption;
        const {
            localErr,
            localErrMsg,
            globalErr,
            globalErrMsg,
            displayErrBlock
        } = this.state;
        const ErrBlock = () => (
            <div>
                {localErr ? <p>{localErrMsg}</p> : null}
                {globalErr ? <p>{globalErrMsg}</p> : null}
            </div>
        );
        return (
            <React.Fragment>
                {this.state.timerType !== "exist" ? (
                    <canvas ref={this.canvasRef} id={id} width={width} height={height}/>
                ) : (
                     <p>{this.state.existMsg}</p>
                 )}
                {displayErrBlock ? <ErrBlock/> : null}
            </React.Fragment>
        );
    }
}

export default Timer;

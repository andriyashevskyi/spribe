import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Timer from "./Timer";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
          <div className="timerContainer">
              <div className="timer-item">
                  <p>Таймер с валидными данными</p>
        <Timer
          localTimer={{
            duration: 15,
            leftValue: 0
          }}
          globalTimer={{
            duration: 120,
            leftValue: 0
          }}
          canvasOption={{
            width: 500,
            height: 500,
            radius: 250,
            id: "myCanvas",
            font:'18px sans-serif',
            textAlign:'center',
            textBaseline:'alphabetic',
            direction:'inherit',
            globalColor:'red',
            localColor:'green',
            backColor:'gray'

          }}
        />
              </div>
              <div className="timer-item">
                  <p> Таймер с ошибкой в локальном таймере</p>
                  <Timer
                      localTimer={{
                          duration: 15,
                          leftValue: 25
                      }}
                      globalTimer={{
                          duration: 120,
                          leftValue: 0
                      }}
                      canvasOption={{
                          width: 500,
                          height: 500,
                          radius: 250,
                          id: "myCanvas",
                          font:'18px sans-serif',
                          textAlign:'center',
                          textBaseline:'alphabetic',
                          direction:'inherit',
                          globalColor:'red',
                          localColor:'green',
                          backColor:'gray'

                      }}
                  />
              </div>
              <div className="timer-item">
                  <p>Таймер с ошибкой в глобальном таймере</p>
                  <Timer
                      localTimer={{
                          duration: 15,
                          leftValue: 0
                      }}
                      globalTimer={{
                          duration: 120,
                          leftValue: 145
                      }}
                      canvasOption={{
                          width: 500,
                          height: 500,
                          radius: 250,
                          id: "myCanvas",
                          font:'18px sans-serif',
                          textAlign:'center',
                          textBaseline:'alphabetic',
                          direction:'inherit',
                          globalColor:'red',
                          localColor:'green',
                          backColor:'gray'

                      }}
                  />
              </div>
              <div className="timer-item">
                  <p>Таймер с ошибкой в глобальном и локальном таймере</p>
                  <Timer
                      localTimer={{
                          duration: 15,
                          leftValue: 35
                      }}
                      globalTimer={{
                          duration: 120,
                          leftValue: 145
                      }}
                      canvasOption={{
                          width: 500,
                          height: 500,
                          radius: 250,
                          id: "myCanvas",
                          font:'18px sans-serif',
                          textAlign:'center',
                          textBaseline:'alphabetic',
                          direction:'inherit',
                          globalColor:'red',
                          localColor:'green',
                          backColor:'gray'

                      }}
                  />
              </div>
          </div>
      </div>
    );
  }
}

export default App;

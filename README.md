This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Установка и запуск

npm install

npm yarn start || npm run start

Рабочий пример:
[https://spribe-47fba.firebaseapp.com/](https://spribe-47fba.firebaseapp.com/)

Timer component props


```javascript
canvasOption:{
	 width: 500, //ширина холста
     height: 500, // высота холста
     radius: 250, // радиус круга
     id: "myCanvas", // id canvas
     font:'18px sans-serif', //шрифт текста
     textAlign:'center', 
     textBaseline:'alphabetic', 
     direction:'inherit',
     globalColor:'red', // цвет глобального таймера
     localColor:'green', // цвет локального таймера
     backColor:'gray' // цвет круга подложки
}

localTimer:{
	duration:120, // продолжительность
	leftValue:0 // уже прошло времени по таймеру
}

globalTimer:{
	durarion:120, // продолжительность
	leftValue:10 // уже прошло времени по таймеру
}

```

